# -*- coding: utf-8 -*-
'''
log 操作
'''
import sys
from flib.tools.console_util import print_with_color
from flib import encoding
import threading
import multiprocessing

def __print__(s, newLine = True, color = None):
    print_with_color(s, newLine=newLine, color=color)

def __log__(tag, color, *args):
    if tag: __print__(encoding.toStr(tag), newLine=False, color=color)
    __index__ = 0
    for s in args:
        __index__ = __index__ + 1
        tmp = encoding.toStr(s)
        __print__(tmp if tmp else str(s), newLine=False, color=color)
        __print__(" " if __index__ != len(args) else "", newLine=False, color=color)
    __print__("", newLine=True, color=color)
    sys.stdout.flush()

_thread_lock = threading.Lock()
_process_lock = multiprocessing.Lock()

def logWithColor(color, *args):
    _process_lock.acquire()
    _thread_lock.acquire()
    __log__("", color, *args)
    _thread_lock.release()
    _process_lock.release()

def log(*args):
    _process_lock.acquire()
    _thread_lock.acquire()
    __log__("", None, *args)
    _thread_lock.release()
    _process_lock.release()

def i(*args):
    _process_lock.acquire()
    _thread_lock.acquire()
    __log__("[info]", None, *args)
    _thread_lock.release()
    _process_lock.release()

def d(*args):
    _process_lock.acquire()
    _thread_lock.acquire()
    __log__("[debug]", "blue", *args)
    _thread_lock.release()
    _process_lock.release()

def w(*args):
    _process_lock.acquire()
    _thread_lock.acquire()
    __log__("[warning]", "yellow", *args)
    _thread_lock.release()
    _process_lock.release()

def e(*args):
    _process_lock.acquire()
    _thread_lock.acquire()
    __log__("[error]", "red", *args)
    _thread_lock.release()
    _process_lock.release()

def s(*args):
    _process_lock.acquire()
    _thread_lock.acquire()
    __log__("[session]", "green", *args)
    _thread_lock.release()
    _process_lock.release()

def expt(*args):
    _process_lock.acquire()
    _thread_lock.acquire()
    err_msg = " ".join([encoding.toStr(x) for x in args])
    _thread_lock.release()
    _process_lock.release()
    raise Exception(err_msg)
