# -*- coding: utf-8 -*-
'''
编码操作
'''

try:
    import chardet
except ImportError:
    from flib import chardet
import sys
is_py3 = sys.version_info >= (3, 0)

def getEncoding(s):
    try:
        enc = chardet.detect(s)
        return enc
    except:
        return

def toUnicode(s):
    if not s:
        return
    if not is_py3 and isinstance(s, unicode):
        return s
    elif is_py3 and isinstance(s, str):
        return s
    try:
        enc = chardet.detect(s)['encoding']
        #print enc
        if enc == 'ISO-8859-2' or enc == "TIS-620":
            enc = 'gbk'
            if not is_py3:
                return unicode(s, enc, 'ignore')
            else:
                return s.decode('utf-8', 'ignore')
    except Exception as e:
        return
    ###
    charsets = ('gbk', 'gb18030', 'gb2312', 'iso-8859-1', 'utf-16', 'utf-8', 'utf-32', 'ascii', 'KOI8-R')
    for charset in charsets:
        try:
            #print charset
            if not is_py3:
                return unicode(s, charset)
            else:
                return s.decode(charset)
        except:
            continue

def toUTF8(s):
    try:
        if not is_py3:
            if not isinstance(s, unicode) and not isinstance(s, str) and not isinstance(s, bytes):
                return
        else:
            if not isinstance(s, str) and not isinstance(s, bytes):
                return
        if not is_py3 and isinstance(s, unicode):
            return s.encode('utf-8')
        elif is_py3 and isinstance(s, str):
            return s.encode('utf-8')
        enc = chardet.detect(s)['encoding']
        if enc == 'utf-8':
            return s
        else:
            s = toUnicode(s)
            if s:
                return s.encode('utf-8')
    except Exception as e:
        return

def toGB2312(s):
    try:
        enc = getEncoding(s)
        if enc and enc['encoding'] == "utf-8":
            s = s.decode('utf-8')
    except Exception as e:
        pass
    s = toUnicode(s)
    if s:
        return s.encode('gb2312')

def toGBK(s):
    try:
        enc = getEncoding(s)
        if enc and enc['encoding'] == "utf-8":
            s = s.decode('utf-8')
    except Exception as e:
        pass
    s = toUnicode(s)
    if s:
        return s.encode('gbk')

def toStr(s):
    if not s: return ""
    try:
        if not is_py3:
            if not isinstance(s, unicode) and not isinstance(s, str) and not isinstance(s, bytes):
                return str(s)
        else:
            if not isinstance(s, str) and not isinstance(s, bytes):
                return str(s)
            if isinstance(s, str):
                return s
            elif isinstance(s, bytes):
                return s.decode('utf-8')
        if sys.stdout.encoding == "cp936":
            return toGBK(s)
        return toUTF8(s)
    except Exception as e:
        return s