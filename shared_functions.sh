# 共享的函数
IS_BUSYBOX=
if [ "$OS" = "Windows_NT" ]; then
	if [ ! "${BASH_SOURCE}" ]; then
		IS_BUSYBOX=true
	fi
elif [ "`uname -s`" = "Darwin" ]; then
	IS_MACOSX=true
fi

# busybox不支持BASH_SOURCE
if [ "$IS_BUSYBOX" = "true" ]; then
	shared_dir=$SHARED_ROOT
else
	shared_dir=$(dirname "${BASH_SOURCE}")
fi

if [ ! "$WINSW_EXECUTABLE" ] && [ ! "$JENKINS_HOME" ]; then
	LOCAL_RUN=true
elif [ "$JENKINS_HOME" ]; then
	JEKINS_RUN=true
fi

find_command()
{
	which $1 > /dev/null 2>&1
	if [ $? == 0 ]; then
	    echo true
	else
	    echo false
	fi
}

find_function()
{
	if [ "$(type -t $1)" = "function" ]; then
		echo true
	elif [ "$IS_BUSYBOX" = "true" ] && [ "$(type -t $1)" = "$1" ]; then
		echo true
	else
		echo false
	fi
}

iconv_op()
{
	if [ "$(find_command iconv)" = "true" ]; then
		iconv ${1+"$@"}
	else
		"$shared_dir/Tools/iconv.exe" ${1+"$@"}
	fi
}

log()
{
	if [ "$IS_BUSYBOX" = "true" ]; then
		echo ${1+"$@"} | iconv_op -f utf-8 -t gbk
	else
		echo ${1+"$@"}
	fi
}
error()
{
	if [ "$IS_BUSYBOX" = "true" ]; then
		log=`echo $@ | iconv_op -f utf-8 -t gbk`
	else
		log="$@"
	fi

	if [ ! $IS_MACOSX ]; then
		echo>&2 -e "\033[;31m$log \033[0m"
	else
		echo>&2 "\033[;31m$log \033[0m"
	fi
}
warn()
{
	if [ "$IS_BUSYBOX" = "true" ]; then
		log=`echo $@ | iconv_op -f utf-8 -t gbk`
	else
		log="$@"
	fi
	if [ ! $IS_MACOSX ]; then
		echo -e "\033[;33m$log \033[0m"
	else
		echo "\033[;33m$log \033[0m"
	fi
}

sessionlog()
{
	if [ "$IS_BUSYBOX" = "true" ]; then
		log=`echo $@ | iconv_op -f utf-8 -t gbk`
	else
		log="$@"
	fi
	if [ ! $IS_MACOSX ]; then
		echo -e "\033[;32m[session] $log \033[0m"
	else
		echo "\033[;32m[session] $log \033[0m"
	fi
}

pause()
{
    #本地运行，暂停提示
    if [ "$LOCAL_RUN" ]; then
		hint="Please Enter Key To Exit."
		if [ ! $IS_MACOSX ]; then
			echo>&2 -n -e "\033[;31m$hint \033[0m\c"
		else
			echo>&2 "\033[;31m$hint \033[0m\c"
		fi
		read
    fi
}

error_pause()
{
	error "$@"
	pause
}

quit_ext()
{
	if [ "$(find_function quit)" = "true" ]; then
		quit ${1+"$@"}
	else
		exit ${1+"$@"}
	fi
}
chmod_op()
{
	chmod ${1+"$@"} || { error_pause "[error] 设置权限失败：chmod $@"; quit_ext 1; }
}

mkdir_op()
{
	mkdir -p ${1+"$@"} || { error_pause "[error] 创建目录失败。mkdir $@"; quit_ext 1; }
}

rm_op()
{
	rm ${1+"$@"} || { sleep 2; rm ${1+"$@"}; } || { sleep 2; rm ${1+"$@"}; } || { error_pause "[error] 删除文件失败：rm $@"; quit_ext 1; }
}

cp_op()
{
	cp ${1+"$@"} || { error_pause "[error] 复制文件失败：cp $@"; quit_ext 1; }
}

mv_op()
{
	mv ${1+"$@"} || { error_pause "[error] 移动文件失败：mv $@"; quit_ext 1; }
}

svn_op()
{
	log "svn $@"
	[ "$tmp" ] || tmp="."
	svn 2>$tmp/svn-stderr --non-interactive ${1+"$@"}
	
	if [ -s $tmp/svn-stderr ]; then
		error "[error] svn 操作失败: svn $@: "
		error_pause "`cat $tmp/svn-stderr`"
		quit_ext 1
	fi
}

xcopy_op()
{
	xcopy ${1+"$@"} | iconv_op -f gbk -t utf-8 ||  { error_pause "[error] xcopy 复制文件失败：xcopy $@"; quit_ext 1; }
}

netcat_op()
{
	log "nc $@"
	if [ "$(find_command nc)" = "true" ]; then
		nc ${1+"$@"} || { error "[error] netcat 操作失败 netcat $@"; }
	else
		"$shared_dir/Tools/busybox.exe" nc ${1+"$@"} || { error "[error] netcat 操作失败 busybox.exe nc $@"; }
	fi
}

unity_op()
{
	log "unity $@"
	
	# 选择一个端口
	port=$(($$ % 65535))
	[ $port -lt 1000 ] && port=$((port+9000))
	[ $port -lt 10000 ] && port=$((port+50000))
	
	log "consoleoutput port: $port"
	
	rm -f "$tmp/unity_ret"
	rm -f "$tmp/console_output_flag"
	
	# 管道前部分：调用 Unity，指定 consoleoutput 地址；把 Unity 返回码写入文件；等一下监听建立；连一下 consoleoutput 监听地址，以免一直等待。管道后部分：监听 consoleoutput 连接，并输出 log
	if [ "$OS" = "Windows_NT" ]; then
			{ "$unity" --consoleoutput "127.0.0.1:$port" -logFile "$tmp/unity.log" ${1+"$@"}; echo "$?">"$tmp/unity_ret"; sleep 1; echo "front">"$tmp/console_output_flag"; warn "[warning] Unity did not connect console output" | netcat_op 127.0.0.1 $port 2>"$tmp/null"; } \
		| { netcat_op -l -p $port 127.0.0.1; echo "back">"$tmp/console_output_flag"; }
	else
			{ "$unity" --consoleoutput "127.0.0.1:$port" -logFile "$tmp/unity.log" ${1+"$@"}; echo "$?">"$tmp/unity_ret"; sleep 1; echo "front">"$tmp/console_output_flag"; warn "[warning] Unity did not connect console output" | netcat_op 127.0.0.1 $port 2>"$tmp/null"; } \
		| { netcat_op -l $port; echo "back">"$tmp/console_output_flag"; }
	fi
	ret=$(cat "$tmp/unity_ret")
	
	console_output_flag=$(cat "$tmp/console_output_flag")
	if [ "$console_output_flag" = "back" ]; then	#管道后部进程后结束，表明无 console output，输出文件 log
		cat "$tmp/unity.log"
	fi
	
	if [ ! "$ret" = "0" ]; then
		error_pause "[error] unity 调用失败：unity $@"
		quit_ext 1
	fi
}

zip_op()
{
	if [ "$(find_command zip)" = "true" ]; then
		zip ${1+"$@"} || { error_pause "[error] zip 失败：zip $@"; quit_ext 1; }
	else
		"$shared_dir/Tools/zip/zip.exe" ${1+"$@"} || { error_pause "[error] zip.exe 失败：zip $@"; quit_ext 1; }
	fi
}
cygpath_op()
{
	"$shared_dir/Tools/rsync/bin/cygpath.exe" ${1+"$@"}
}

busybox_op()
{
	"$shared_dir/Tools/busybox.exe" ${1+"$@"}
}

# sync_op <src> <dest> [options ...]。效果同 rsync
sync_op()
{
	if [ "$OS" = "Windows_NT" ]; then
		if [ "$IS_BUSYBOX" = "true" ]; then
			mkdir_op "$2"
			src=$(cygpath_op "$1")	
			dest=$(cygpath_op "$2")
			shift
			shift
			rsync_bin="$shared_dir/Tools/rsync/bin/rsync.exe"
			"$rsync_bin" "$src" "$dest" $@ || { sleep 2; "$rsync_bin" "$src" "$dest" $@; } || { sleep 2; "$rsync_bin" "$src" "$dest" $@; } || { error_pause "[error] rsync 同步文件失败：rsync.exe "$src" "$dest" $@"; quit_ext 1; }
		else
			"$shared_dir/Tools/busybox.exe" sh "$shared_dir/Tools/rsync/rsync.sh" ${1+"$@"} || { error_pause "[error] rsync 失败 $@"; quit_ext 1; }
		fi
	else
		src=$1
		dest=$2
		shift
		shift
		rsync "$src" "$dest" $@ || { sleep 2; rsync "$src" "$dest" $@; } || { sleep 2; rsync "$src" "$dest" $@; } || { error_pause "[error] rsync 同步文件失败：rsync "$src" "$dest" $@"; quit_ext 1; }
	fi
}

curl_op()
{
	if [ ! "$(find_command curl)" = "true" ]; then
		error_pause "[error] curl 工具未找到"
		quit_ext 1
	fi
	curl -i -s -S ${1+"$@"} | iconv -f gbk -t utf-8
	if [ ! $? ]; then
		error_pause "[error] curl 操作失败: curl $@"
		quit_ext 1
	fi
}
# 应用差分
apply_diff_op()
{
	project_path=${1%/}
	diff_name=$2
	sub_path=$3

	oldIFS=$IFS
	IFS=","
	for diff_part in $diff_name
	do
		[ "$oldIFS" ] && { IFS=$oldIFS; oldIFS=""; }
		
		diff_dir="$project_path/DiffConfigs/$diff_part"
		
		[ -d "$diff_dir" ] || { error_pause "[error] 差分目录不存在 $diff_dir"; quit_ext 1; }
		
		if [ ! "$sub_path" ]; then
			cp_op -rf "$diff_dir/"* "$project_path/"
		else
			cp_op -rf "$diff_dir/$sub_path/"* "$project_path/$sub_path/"
		fi
	done
}

#所有文件名和目录都转成小写
tolower_op()
{
	dir_tolower()
	{
		for file in `ls $1 | grep '[A-Z]'`
		do
			str=`echo $file|tr 'A-Z' 'a-z'`
			mv -f $1/$file $1/$str || { error_pause "[error] tolower 修改文件名为小写。"; quit_ext 1; }
		done
	}
	dir_tolower $1
	for file in $1/* 
	do
		if [ -d $file ]; then
			tolower_op $file
		fi
	done
}

lower_op()
{
	#可以统一全用shell
	if [ "$OS" = "Windows_NT" ]; then
		"$shared_dir/Tools/afiletools/tolower.exe" ${1+"$@"} || { error_pause "[error] lower 修改文件名为小写 tolower.exe $@"; quit_ext 1; }
	else
		tolower_op ${1+"$@"} || { error_pause "[error] lower 修改文件名为小写 tolower $@"; quit_ext 1; }
	fi
}

isworkcopy_op()
{
	svn info $1 > /dev/null 2>&1
	if [ $? == 0 ]; then
	    echo true
	else
	    echo false
	fi
}

svnremovemissing()
{
	FILES=
	removemissing()
	{
		STATUS_URL="svn_op status --ignore-externals $1"
		STATUS_NUM=`${STATUS_URL} |wc -l`
		if [ ${STATUS_NUM} -ne 0 ]; then
			STATUS_LIST=`${STATUS_URL}`
			NUM=0
			HANDLE=0
			FLAG=
			for FIELD in ${STATUS_LIST} ; do
				if [ ${#FIELD} -lt 3 ]; then
					FLAG=$FIELD
					#!是missing,?是本地文件，X是外链目录,D是删除状态
					if [ "${FIELD}" == "!" ]; then
						#下一个应该处理
						let NUM+=1
						HANDLE=1
					# elif [ "${FIELD}" == "?" ]; then
					# 	let NUM+=1
					# 	HANDLE=1
					elif [ "${FIELD}" == "X" ]; then
						let NUM+=1
						HANDLE=1
					# elif [ "${FIELD}" == "D" ]; then
					# 	let NUM+=1
					# 	HANDLE=1
					fi
					continue
				fi

				#若为删除文件则不必导出
				if [ ! ${HANDLE} -eq 1 ]; then
					continue
				fi
				if [ "$OS" = "Windows_NT" ]; then
					FILE=$(echo "$FIELD" | sed -r 's/\//\\/g')
				else
					FILE=$FIELD
				fi
				HANDLE=0
				if [ "$FLAG" = "X" ]; then
					removemissing ${FILE}
				elif [ "$FLAG" = "?" ]; then
					rm_op -rf ${FILE}
				elif [ "$FLAG" = "!" ]; then
					svn_op delete -q --force ${FILE}
					FILES=${FILES}" ${FILE}"
				elif [ "$FLAG" = "D" ]; then
					FILES=${FILES}" ${FILE}"
				fi
			done
		fi
	}

	if [ ! "$(isworkcopy_op $1)" = "true" ]; then
		error_pause "path is not svn working folder: $1"
		quit_ext 1
	fi
	removemissing $1
	#[ "$FILES" ] && svn_op commit -m "auto removemissing" $FILES
}


svnremovemissing_op()
{
	#可以统一全用shell
	if [ "$OS" = "Windows_NT" ]; then
		"$shared_dir/Tools/ResourceUpdatePack/bin/Release/svnremovemissing.exe" ${1+"$@"} || { error_pause "[error] svnremovemissing.exe $@"; quit_ext 1; }
	else
		svnremovemissing ${1+"$@"} || { error_pause "[error] svnremovemissing $@"; quit_ext 1; }
	fi
}

resourceupdatepack_op()
{
	#这个函数内部不允许其他输出
	if [ "$OS" = "Windows_NT" ]; then
		"$shared_dir/Tools/ResourceUpdatePack/bin/Release/resourceupdatepack.exe" ${1+"$@"} || { error_pause "[error] 资源更新打包失败：resourceupdatepack $@"; quit_ext 1; }
	else
		python "$shared_dir/Tools/ResourceUpdatePack/bin/Release/resourceupdatepack.py" ${1+"$@"} || { error_pause "[error] 资源更新打包失败：resourceupdatepack $@"; quit_ext 1; }
	fi
}

get_resourceversion_op()
{
	resource_svn=${1%/}
	if [ ! "$2" = "base" ]; then
		resourceupdatepack_op --uri=$resource_svn --latest
	else
		base_version=$(resourceupdatepack_op --uri=$resource_svn --base)
		#echo "$base_version" | awk -F': ' '{print $2}'
		echo "$base_version"
	fi
}

compile_lua_op()
{
	python "$shared_dir/Tools/LuaCompiler/compile_lua.py" ${1+"$@"} || { error_pause "[error] Lua脚本编译失败 compile_lua $@"; quit_ext 1; }
}

xcodebuild_op()
{
	log xcodebuild {1+"$@"}
	xcodebuild ${1+"$@"} || { error_pause "[error] xcodebuild 操作失败：xcodebuild $@"; quit_ext 1; }
}

packpng_op()
{
	if [ "$OS" = "Windows_NT" ]; then
		"$shared_dir/Tools/afiletools/PackTool.exe" ${1+"$@"} || { error_pause "[error] 生成文件包失败：PackTool.exe $@"; quit_ext 1; }
	else
		"$shared_dir/Tools/afiletools/PackTool" ${1+"$@"} || { error_pause "[error] 生成文件包失败：PackTool $@"; quit_ext 1; }
	fi
}

# 计算文件md5
calcfile_md5_op()
{
	if [ "$(find_command md5sum)" = "true" ]; then
		echo `md5sum $1 | awk -F " " '{print $1}'`
	elif [ "$(find_command md5)" = "true" ]; then
		echo `md5 $1 | awk -F " = " '{print $2}'`
	else
		echo ""
	fi
}
#oldmd5code=$(calcfile_md5_op "$out_dir/PB/client_msg.lua")

abspath_op()
{
	# 给定某个路径，输出绝对路径
	# $1 给定路径
	src_path=$1

	if [ ! "$src_path" ]; then
		base_name=`basename "$0"`
		echo "usage: $base_name <路径>"
	elif [ "${src_path#/}" = "$src_path" ] && [ "${src_path#\\}" = "$src_path" ] && [ "${src_path#*:}" = "$src_path" ]; then	#不以 '/', '\' 开头，且不含 ':'，是相对路径
		echo "$(pwd)/$src_path"
	else
		echo "$src_path"
	fi
}

# RunUAT.bat, 需要指定UE_ROOT
UAT_op()
{
	log "RunUAT $@"
	
	if [ ! $UE_ROOT ]; then
		error_pause "UE_ROOT未定义"
		quit_ext 1
	fi
	if [ ! "$MY_UE_CONSOLE" = "true" ]; then
		if [ ! $IS_MACOSX ]; then
			"$UE_ROOT/Engine/Build/BatchFiles/RunUAT.bat" ${1+"$@"} || { error_pause "[error] RunUAT.bat失败。RunUAT.bat $@"; quit_ext 1; }
		else
			"$UE_ROOT/Engine/Build/BatchFiles/RunUAT.sh" ${1+"$@"} || { error_pause "[error] RunUAT.sh失败。RunUAT.sh $@"; quit_ext 1; }
		fi
	else
		python "$shared_dir/Tools/Scripts/ue.py" "$UE_ROOT" "UAT" ${1+"$@"} || { error_pause "[error] python UAT失败。$@"; quit_ext 1; }
	fi
}
# UnrealBuildTool.exe, 需要指定UE_ROOT
UBT_op()
{
	log "UnrealBuildTool $@"
	if [ ! $UE_ROOT ]; then
		error_pause "UE_ROOT未定义"
		quit_ext 1
	fi

	if [ ! "$MY_UE_CONSOLE" = "true" ]; then
		if [ ! $IS_MACOSX ]; then
			"$UE_ROOT/Engine/Binaries/DotNET/UnrealBuildTool.exe" ${1+"$@"} || { error_pause "[error] UnrealBuildTool.exe失败。UnrealBuildTool.exe $@"; quit_ext 1; }
		else
			sh "$UE_ROOT/Engine/Build/BatchFiles/Mac/RunMono.sh" "$UE_ROOT/Engine/Binaries/DotNET/UnrealBuildTool.exe" ${1+"$@"} || { error_pause "[error] UnrealBuildTool.exe失败。UnrealBuildTool.exe $@"; quit_ext 1; }
		fi
	else
		python "$shared_dir/Tools/Scripts/ue.py" "$UE_ROOT" "UBT" ${1+"$@"} || { error_pause "[error] python UBT失败。$@"; quit_ext 1; }
	fi
}
# UE4Editor-Cmd.exe, 需要指定UE_ROOT
UE_op()
{
	log "UE4Editor-Cmd $@"
	if [ ! $UE_ROOT ]; then
		error_pause "UE_ROOT未定义"
		quit_ext 1
	fi

	if [ ! "$MY_UE_CONSOLE" = "true" ]; then
		if [ ! $IS_MACOSX ]; then
			"$UE_ROOT/Engine/Binaries/Win64/UE4Editor-Cmd.exe" ${1+"$@"} || { error_pause "[error] UE4Editor-Cmd.exe失败。UE4Editor-Cmd.exe $@"; quit_ext 1; }
		else
			"$UE_ROOT/Engine/Binaries/Mac/UE4Editor.app" ${1+"$@"} || { error_pause "[error] UE4Editor-Cmd.exe失败。UE4Editor.app $@"; quit_ext 1; }
		fi
	else
		python "$shared_dir/Tools/Scripts/ue.py" "$UE_ROOT" "CMD" ${1+"$@"} || { error_pause "[error] python UECmd失败。$@"; quit_ext 1; }
	fi
}

# 生成资源列表
gen_reslist_op()
{
	log "SUExporter $@"
	old_path=`pwd`
	cd "$shared_dir/Tools/SUExporter" && "$shared_dir/Tools/SUExporter/SUExporter.exe" && cd $old_path || { error_pause "[error] SUExporter.exe失败。SUExporter.exe $@"; quit_ext 1; }
}

strip_r_op()
{
	python "$shared_dir/Tools/Scripts/strip.py" "$1"
}

resource_update_op()
{
	resource_update_svn=$1
	resource_update_release_mode=$2
	resource_update_cache=$3
	resource_update_upload_dir=$4

	if [ ! "$resource_update_svn" ]; then
		error_pause "[error] 未给出 resource-update-svn"
		quit_ext 1
	fi

	if [ ! "$resource_update_cache" ]; then
		error_pause "[error] 未给出 resource-update-cache"
		quit_ext 1
	fi

	if [ ! "$resource_update_upload_dir" ]; then
		error_pause "[error] 未给出 resource-update-upload-dir"
		quit_ext 1
	fi

	mkdir_op "$resource_update_cache"
	log "======================================================="
	log "生成更新包 `date '+%F %T'`"
	[ "$resource_update_release_mode" = "true" ] && publish="--publish"
	rm_op -f "${resource_update_cache}_packlist/packlist.txt"
	resourceupdatepack_op --uri=$resource_update_svn --output=$resource_update_cache --delete --packlist $publish

	log "上传更新包到：$resource_update_upload_dir `date '+%F %T'`"
	if [ -f "${resource_update_cache}_packlist/packlist.txt" ]; then
		for package in `cat "${resource_update_cache}_packlist/packlist.txt"`
		do
			spackage=$(strip_r_op "$package") || quit_ext 1
			log "    上传 $spackage `date '+%F %T'`"
			cp_op -rf "$resource_update_cache/$spackage" "$resource_update_upload_dir/"
		done
		cp_op -f "$resource_update_cache/version.txt" "$resource_update_upload_dir/"
	else
		sync_op "$resource_update_cache/" "$resource_update_upload_dir" -r -q -W # -c --delete 
	fi




	log "资源更新完成 `date '+%F %T'`"
	log "======================================================="
}

packone_op()
{
	in_path=$1
	out_path=$2
	log "正在生成：$out_path"
	rm_op -f "$out_path"
	packpng_op -i $in_path -o $out_path -lz4 uasset -lz4 uexp -lz4 ubulk -lz4 umap -lz4 ufont -lz4 ushaderbytecode -lz4 ushaderpipeline -0 bank
}

# 打包所有的资源到png
pckpackall_op()
{
	input=$1
	output=$2
	usesub=$3
	ext=".png"
	packone()
	{
		pck_name=$1
		in_path="$input/$pck_name"
		out_path="$output/$pck_name$ext"

		packone_op $in_path $out_path
	}

	if [ "$usesub" = "true" ]; then
		packone lua
		packone ui
		packone configs
		packone sound
		packone shadercode
		packone engine
	else
		if [ -d "$input/cinematics" ]; then
			packone cinematics
		fi
		if [ ! $platform ] || [ "$platform" = "win" ]; then
			if [ -d "$input/Procedural_Ecosystem" ]; then
				packone procedural_ecosystem
			fi
			if [ -d "$input/Templates" ]; then
				packone templates
			fi
			if [ -d "$input/WaterMill" ]; then
				packone watermill
			fi
		elif [ -d "$input/Procedural_Ecosystem" ] || [ -d "$input/Templates" ] || [ -d "$input/WaterMill" ]; then
			error "[error] $platform平台资源中有对Procedural_Ecosystem，Templates和WaterMill的引用"
			error_pause "[error] pckpackall_op失败。pckpackall_op $@"
			quit_ext 1
		fi

		packone automove
		packone buildings
		packone configs
		packone data
		packone effects
		packone engine
		packone lua
		packone maps
		packone miscs
		packone models
		packone scenes
		packone shadercode
		packone sound
		packone ui
	fi
}

#xmake 编译
xmake_op()
{
	if [ ! "$XMAKE_HOME" ]; then
		source "$shared_dir/Tools/get_xmake.sh"
	fi
	if [ ! "$XMAKE_HOME" ]; then
		error "XMAKE_HOME Not Define."
		quit_ext 1
	fi

	if [ "$OS" = "Windows_NT" ]; then
		"$XMAKE_HOME/xmake.exe" ${1+"$@"} || { error "error build"; quit_ext 1; }
	else
		"$XMAKE_HOME/xmake" ${1+"$@"} || { error "error build"; quit_ext 1; }
	fi
}

msbuild_op()
{
	log "msbuild $@"
	if [ "$LOCAL_RUN" ]; then
		"$msbuild" ${1+"$@"} #|| { error_pause "[error] msbuild失败。msbuild $@"; quit_ext 1; }
	else
		"$msbuild" ${1+"$@"} | iconv_op -f gbk -t utf-8
	fi
	ret_code=$?
  	if [ $ret_code -ne 0 ]; then
  		error_pause "[error] msbuild失败。msbuild $@"
  		quit_ext 1
  	fi
}

get_msbuild_op()
{
	if [ "$1" = "csharp" ]; then
		if [ -f "$WINDIR/Microsoft.NET/Framework/v4.0.30319/MSBuild.exe" ]; then
			MS_BUILD="$WINDIR/Microsoft.NET/Framework/v4.0.30319/MSBuild.exe"
		fi
	else
		if [ -d "$VS141COMNTOOLS" ]; then
			COMNTOOLS=${VS141COMNTOOLS%/}
			MS_BUILD="$COMNTOOLS/../../MSBuild/15.0/Bin/MSBuild.exe"
		elif [ -f "${ProgramFiles}/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe" ]; then
			MS_BUILD="${ProgramFiles}/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe"
		elif [ -f "${ProgramFiles(x86)}/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe" ]; then
			MS_BUILD="${ProgramFiles(x86)}/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe"
		fi
	fi
}

sendrtxmsg_op()
{
	log "send rtx msg: $@"
	python "$shared_dir/Tools/Scripts/rtxclient.py" ${1+"$@"}
}

get_current_ue4_version()
{
	if [ -f "$UE_ROOT/ue4.version" ]; then
		echo "`cat $UE_ROOT/ue4.version`"
	fi
}
get_latest_ue4_version()
{
	if [ -f "//10.236.100.35/CommonShare/software/UE4_Editor/Editor/ue4.version" ]; then
		echo "`cat //10.236.100.35/CommonShare/software/UE4_Editor/Editor/ue4.version`"
	fi
}

check_ue4_version()
{
	if [ ! $UE_ROOT ]; then
		error_pause "UE_ROOT未定义"
		quit_ext 1
	fi
	cur_ue4_version=$(get_current_ue4_version)
	latest_ue4_version=$(get_latest_ue4_version)
	if [ ! "$cur_ue4_version" = "$latest_ue4_version" ]; then
		[ "$cur_ue4_version" ] || cur_ue4_version="(Unknow)"
		message="UE4Engine版本不匹配，当前本地版本$cur_ue4_version, 服务器版本$latest_ue4_version。"
		warn "$message"
		if [ "$LOCAL_RUN" ] && [ "$OS" = "Windows_NT" ]; then
			content=`echo ${message}点击确定坚持打开？| iconv_op -f utf-8 -t gbk`
			title=`echo [警告] | iconv_op -f utf-8 -t gbk`
			"$shared_dir/Tools/MsgBox.exe" -content "${content}" -title "$title"
			ret_code=$?
			#返回值1： MB_OK, 2: MB_CANCEL
			if [ $ret_code -eq 1 ]; then
				warn "您的本地UE4引擎版本比较旧，请尽快更新到最新，更新地址//10.236.100.35/CommonShare/software/UE4_Editor/Editor。"
			elif [ $ret_code -eq 2 ]; then
				quit_ext 1
			fi
			content=
			title=
		elif [ "$LOCAL_RUN" ]; then
			hint="请输入[y/n]选择是否继续："
			if [ ! $IS_MACOSX ]; then
				echo -n -e "\033[5;31m$hint \033[0m\c"
			else
				echo "\033[5;31m$hint \033[0m\c"
			fi
			hint=
			read answer
			if [ ! "$answer" = "Y" ] && [ ! "$answer" = "y" ]; then
				quit_ext 1
			else
				warn "您的本地UE4引擎版本比较旧，请尽快更新到最新，更新地址//10.236.100.35/CommonShare/software/UE4_Editor/Editor。"
			fi
		else
			log "UE4Engine版本不匹配，当前本地版本$cur_ue4_version, 服务器版本$latest_ue4_version。"
		fi
		message=
	else
		log "当前UE4引擎版本是：$cur_ue4_version"
	fi
}

buildconsole_op()
{
	log "BuildConsole: $@"
	if [ "$LOCAL_RUN" ]; then
		BuildConsole ${1+"$@"}
	else
		BuildConsole ${1+"$@"} | iconv_op -f utf-8 -t gbk
	fi
	ret_code=$?
  	if [ $ret_code -ne 0 ]; then
  		error_pause "[error] BuildConsole失败。BuildConsole $@"
  		quit_ext 1
  	fi
}